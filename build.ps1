[cmdletbinding()]
Param (
    [ValidateSet('Build','Preview')]
    [string]$Task         = 'Build',
    [string]$HugoVersion  = '0.25.1',
    [string]$HugoSha      = '5d89bad43a4a7641ac827c1492682e97936711f4077255d2fefd94d0c387a831',
    [string]$ThemeName    = 'hugo-theme-nix',
    [string]$ThemeUrl      = 'https://github.com/LordMathis/hugo-theme-nix',
    [switch]$Clean
)

If ($Clean -eq $true) {
    Remove-Item -Path "$PSScriptRoot/hugo*","$PSScriptRoot/themes","$PSScriptRoot/public" -Recurse -Force -ErrorAction SilentlyContinue
}

# Download Hugo locally
If (!(Test-Path -Path "$PSScriptRoot/hugo.exe")) {
    $DownloadUrl = "https://github.com/gohugoio/hugo/releases/download/v${HugoVersion}/hugo_${HugoVersion}_Windows-64bit.zip"
    Invoke-WebRequest -Uri $DownloadUrl -UseBasicParsing -OutFile "${HugoVersion}.zip"
    # Extract the hugo binary to the project root for future use
    Expand-Archive -Path "$PSScriptRoot/${HugoVersion}.zip" -DestinationPath $PSScriptRoot
    # Clean up the zip and extra files
    Remove-Item "$PSScriptRoot/${HugoVersion}.zip","$PSScriptRoot/*.md"
    # Verify hugo runs correctly
    & $PSScriptRoot/hugo.exe version
}

# Download the theme if necessary
If (!(Test-Path -Path "$PSScriptRoot/themes/${ThemeName}" -PathType Container)) {
    If (!(Test-Path -Path "$PSScriptRoot/themes")) {
        $null = New-Item -Path "$PSScriptRoot/themes" -ItemType Directory
    }
    Push-Location -Path "$PSScriptRoot/themes"
    git clone $ThemeUrl
    Pop-Location
    # Invoke-WebRequest -Uri $ThemeZipUrl -OutFile "$PSScriptRoot/${ThemeName}.zip"
    # Expand-Archive -Path "$PSScriptRoot/${ThemeName}.zip" -DestinationPath "$PSScriptRoot/themes"
    # Remove-Item -Path "$PSScriptRoot/${ThemeName}.zip"
    # Rename-Item -Path (Get-Item -Path "$PSScriptRoot/Themes/${ThemeName}*").FullName -NewName $ThemeName
}

Switch ($Task) {
    'Preview' {
        & $PSScriptRoot/hugo.exe server
    }
    Default {
        & $PSScriptRoot/hugo.exe
    }
}