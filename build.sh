#!/bin/bash
if [ ! -e hugo ]
then
    # Download Hugo
    wget -O ${HUGO_VERSION}.tar.gz https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/hugo_${HUGO_VERSION}_Linux-64bit.tar.gz
    # Verify Checksum
    echo "${HUGO_SHA}  ${HUGO_VERSION}.tar.gz" | sha256sum -c
    # Expand the archive and delete it, as well as the license and readmes.
    tar xf ${HUGO_VERSION}.tar.gz && rm ${HUGO_VERSION}.tar.gz
    rm *.md
    # Verify hugo runs properly
    ./hugo version
fi

if [ ! "$(ls -A themes/${THEME_NAME})" ]
then
    # Create the themes folder
    mkdir themes
    # Download the latest theme
    cd themes
    git clone ${THEME_URL}
    cd ..
fi

if [ -z $1 ] || [ $1 = "build" ]
then
    ./hugo
elif [ $1 = "preview" ]
then
    ./hugo server
fi
