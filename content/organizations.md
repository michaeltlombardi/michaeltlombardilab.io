+++
title = "Organizations"
date = "2017-07-08"
sidemenu = "true"
description = "Organizations I belong to"
+++

## [St. Louis PowerShell User Group][stlpsug] (_Founder, co-organizer_)

I founded the STLPSUG along with [Josh Castillo][castillo] in January of 2016.
Since then, I've been an organizer for the group and scheduling, running, and speaking at our events with the help of co-organizer [Ken Maglio][maglio]

We have a [twitter account][stlpsug-twitter] where you can reach us if you're interested in attending, speaking, or have questions.
We encourage you to RSVP to a [meetup][stlpsug] if you're in the area!

You can also always [email us][stlpsug-email] or contact me directly.

[stlpsug]:         https://meetup.com/stlpsug
[stlpsug-twitter]: https://twitter.com/stlpsug
[stlpsug-email]:   mailto:stl.psug@outlook.com
[castillo]:        https://twitter.com/doesitscript
[maglio]:          https://twitter.com/kenmaglio
