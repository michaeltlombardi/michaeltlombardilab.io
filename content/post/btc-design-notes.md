---
author: "Michael T Lombardi"
date: 2019-06-03
linktitle: Beneath the Canals Design Notes
# menu:
#   main:
#     parent: tutorials
# next: /tutorials/github-pages-blog
# prev: /tutorials/automated-deployments
title: Beneath the Canals Design Notes
# weight: 10
---

While talking to [Jason Tocci](https://pretendo.games/) on [Twitter](https://twitter.com/JasonT/status/1135688601965158400) earlier today I realized that I hadn't included the design notes in the [_Beneath the Canals_](https://www.kickstarter.com/projects/michaeltlombardi/beneath-the-canals/) zini.

> Hey! Did you miss the kickstarter? Did you know you can snag a copy of _Beneath the Canals_, **right now**, [on LeanPub](https://leanpub.com/beneath-the-canals)?
> If you wait until the rest of the zinis are finished, you'll be able to buy the bundle there too, which includes 14 other zinis, for 40% off!

For a bit of background, when I do design work I tend to include as much supporting info about how something is supposed to work as I can reasonably fit - you know, without making things too unwieldy.
I do this because I believe strongly in kitbash design, the stealing, tweaking, replacing, and otherwise DIY roll-up-your-sleeves-and-change-stuff behavior I think makes for the best games _at your table_.

So!
With kitbashing and that conversation in mind, onto the mechanics!

## The Room Generator

The Room Generator table sort of sets up the rest of the tooling by telling you:

- How dangerous, generally, the area you're in is (the _category_ of the room: Undercity, Ancient, Opulent, or Volcanic)
- The type of room (Stairs, Passage, Grotto, Chamber, Vault, or Tomb)
- What random feature makes that room more interesting (list of 10 including ambient glows, blood stains, an ossuary, a hideout, etc.)

The category also determines the size of the die you roll for encounters - the more dangerous the area, the larger the die.

> As we'll see later, the more strange and dangerous encounters are higher on the encounter table, so a lower roll leads to more mundane or safe encounters.

The room itself has a modifier for the type of feature you're likely to find - this was intended to make it hard or impossible, for example, to have stairs with a sarcophagus on them or a tomb whose only interesting feature is water on the floor.

> You could tweak this by creating your own list of room features and positioning them such that larger/more impressive rooms are more likely to have interesting features.
> You could also drop this modifier entirely to have hideouts on stairways and figure out how to make that fit the fiction!

## Time of Day

This table was included explicitly to try to share world lore in a way that has some sort of mechanical impact.
Pentola is a megalopolis on a toroidal (donut-shaped) planet with a moon bobbing perfectly up and down through the center of it.

It spins very quickly to maintain something of a reasonable gravity and to stay stable as a planet, so it actually has three alternating periods of sunlight and darkness in a single 24 hour period, meaning there's three sunsets per day!

Instead of counting their days so much by the rising/setting of the sun, they watch for the rising and setting of the moon.
Unlike the sun however, the moon over Pentola is _ominous_ and seems to make magic and monsters more powerful and dangerous.

So this table affects that by adding a flat bonus to your roll on the encounter table - the higher the moon, the higher the bonus.
This _should_ skew your encounters during the Quietdark towards the weird and horrifying as most folks take shelter to wait the period out.

> You could use these same bonuses for magical effects, empowering spells or items or granting additional bonuses to magical creatures.
> You could also make these effects more dramatic in and out of direct moonlight if you end up in an area on the surface.

## Encounters

This table is complex and nested and about as dense as I could make it.
The first set of entries are omens that should ratchet up the tension a little - exploring the catacombs should be a little frightening - remember, they're _unmappable_, the only way to explore them is blindly.
If you're *very* lucky, you can find a guide to help you traverse them.

> I made the catacombs unmappable to again smash lore and mechanics together;
> I wanted an excuse to procedurally generate whole swaths of the territory beneath the megalopolis and making it unmappable also shows the immense magics of the Old People.

After the omens the next set of encounters - Unsavory Sneaker, Mercantile, and Corpseants - are all the sorts of things you semi-regularly discover in the Undercity (which rolls a d4 on this table).
I wanted it to be more likely to run into other people the closer you are to the surface and fill this swath with not-necessarily-hostile encounters.

The encounter table overall heavily leans towards "violence happens if and only if the party is very unlucky or very bloodthirsty" - most of the animal encounters are non-violent or can be avoided, only two rolls will land you face to face with a shard - and even then a third of those encounters can be fled.

> This table could be modified to focus more on the Undercity or the tombs, adding or removing human interactions and altering the behavior of discovered creatures.
> If you're going to modify this table you may want to alter only parts of it at a time - maybe add another set of omens and drop the shard ambush, for example.

## Shards

The shards are the terrifying and tragic monsters, the otherworldly demons, which haunt the Catacombs.

There's over a thousand random configurations the shards can take on, all of which include details that make them dangerous or alien.
They should remain just human enough to see them as corrupted beings, dark mirrors of player emotions.

They're meant to keep pressure on the players when exploring the deep places of the city - linger too long, too deep, and they'll find you.

> If you're adding or replacing forms or details, you'll want them to remain clearly humanoid and pick some detail to make them scary.
> It's best to stay away from anything that codes as a disability - make the monstrous features something truly otherworldy instead of relying on tropes which are, at their core, ableist.
>
> If you're adding or replacing abilities they're largely intentionally triggered when someone does something _to_ the shard or are abilities which leave signs of the shard behind or are useful outside of direct violence.
> The abilities should be the sorts of things that will frighten characters - shards are the embodiment of pain and horror and suffering made magical and inhuman.

## Magic Items

The magic item generation rules largely exist to help you quickly scaffold interesting items that aren't just bonuses to a stat or ability or skill as well as imparting more of the setting to you;
iron is precious, there's more art and non-panoply items beneath the canals than wargear, a plethora of magic and strange abilities.

The generation of the items is meant to help shortcut things for _you_, the referee - but even still, it's relatively slow.

> I don't have many good suggestions for modifying this table except that you could reuse the structure to teach about your own setting:
>
> - Replace the themes with those most important to the people of your locale
> - Replace the decorative materials with ones that have meaning or rarity in your setting
> - Replace the details with more/less weird ones, or lean into a particular theme (opulence or strange omens, perhaps)
> - Replace the effects with a list that makes sense for the types of items you want to generate - this list doesn't include necromancy because that's not a thing in Pentola, but maybe it is in your world!

## Wrapping Up

I try (and am not always successful) to design tools which are kitbashable.
I hope these design notes, coupled with the zini itself, empower you to modify, remix, and otherwise kitbash my work to make sense for _your_ table.

Cheers!
