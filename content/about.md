+++
title = "About"
date = "2017-06-05"
sidemenu = "true"
description = "About me and this site"
+++

# Who
I'm Mike Lombardi, a human who works on systems and tries to continually improve them.
I started my career doing application-level sysadmin work and have ended up as an software engineer.

My primary goal is to be a force multiplier for organizations and communities by documenting, mentoring, and working to continuously improve processes and tooling.
In a work context, I'm passionate about restorative justice and making work more fair and less stressful.

I spend a lot of my spare cycles worldbuilding, playing tabletop RPGs, and learning new things.
I realized that having worldbuilding as a hobby is a great excuse to research all the things, and it sounds much better than "novelist who hasn't written anything worth reading yet." <i class="fa fa-smile-o" aria-hidden="true"></i>

# Why

A few years ago, I was working operations for a designated-critical application and when things went wrong it was hellish.
Long hours, painful after-actions, lots of finger pointing, failed deployments, low trust, lots of manual work.

I was stressed out and upset a lot of the time and I came across articles and talked with people that really hammered home a few things to me:

1. Stress has a very real, very detrimental impact on health, especially when it's steady over a long period.
2. Operations folks, especially in 'mission-critical' roles (security, finance, health), tend to commit suicide at higher rates than the background.
3. There are ways to run IT operations that are less stressful and painful for folks.

I went looking for those better ways, determined to at least see if we could make our own work more humane, and found the DevOps movement that way.

Personally, I think it's _neat_ that shifting our businesses to be more humane **also** means we can deliver more value faster with fewer defects, but it's the impact on the _people_ that I am most invested in.

My career since then has been about making IT operations more humane for everyone else in the industry.
It's the reason I started a [user group][stlpsug], mentor folks, co-organize a [community lighting talk platform][pspowerhour], and why I write yell things on [Twitter][twitter].

[stlpsug]:     https://meetup.com/stlpsug
[pspowerhour]: https://youtube.com/c/pspowerhour
[twitter]:     https://twitter.com/barabariankb