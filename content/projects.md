+++
title = "Projects"
date = "2014-04-09"
sidemenu = "true"
description = "Projects I maintain or contribute to"
+++

---

## Writing

> ### [PowerShell Conference Book][psconfbook] (_Editor, Contributing Author_)
>
> The PowerShell Conference Book (#psconfbook) is a collaborative project by over thirty members of the PowerShell community - speakers, bloggers, writers, and user group organizers - to each write a deep-dive topic that PowerShell users and community members would be interested in, spanning everything from managing legacy code to ChatOps to writing secure code to DevOps.
>
> The project exists to give back to the community - first, by sharing knowledge and by helping other folks learn hard lessons the easier way. Secondly, and most importantly, but helping the next group of folks get into the industry and get ready to take our place.
>
> Every single royalty dollar of this project is donated to a charity fund held by [The DevOps Collective][devopscollective] to benefit the [OnRamp][onramp] program's [scholarship][onramp-scholarship] fund.
> The OnRamp program provides folks new to the field of IT Operations advanced hands-on training with top experts in the field, networking with hundreds of professionals, and their own dedicated mentor.
> It has additional scholarships earmarked for folks who are underrepresented in the IT Operations community.
>
> In the first six months after release the PowerShell Conference Book was able to _fully fund_ three scholarships and 75% of a fourth.
> All royalties continue to benefit this scholarship fund.
>
> This project was led by [Mike F Robbins][mikefrobbins] with [Jeff Hicks][jeffhicks] as senior editor.

> ### [pwshop: A PowerShell 101 Workshop][pwshop] (_Author, Instructor_)
>
> `pwshop` (pronounced 'power-shop') is a PowerShell 101 workshop available on LeanPub which is designed to take someone who has never opened a PowerShell prompt before and empower them to be able to work in the shell to get day-to-day work done.
> It covers the fundamentals with an eye towards ensuring students will be able to develop skills that they can use to help themselves advance further - getting help, inspecting objects, running commands, managing remote systems, etc. - and includes numerous exercises, instructor attention, and a certificate of completion.
>
> `pwshop` also includes audio files for each chapter and exercise, narrated by the wonderful [Chris Allen][chrisallen].
> We intend, once complete, to publish it in full audio format.

> ### [Needful Docs][needful] (_Maintainer_)
>
> Needful-Docs is a [gitbook][gitbook] project I wrote to be a helpful resource for open source maintainers to refer to when writing or talking about documentation.
> It explains the three types of documentation and gives some advice and tips for writing them.
>
> This project was an example of _presentation-driven-development_ - I used it as my slides for my lightning talk at the [PowerShell and DevOps North America Summit 2017][pssummit-2017].

> ### [Pentola: City of Shards][pentola] (Author)
>
> _Pentola: City of Shards_ is a tabletop roleplaying game and setting.
> It's a system and setting that leans hard into [picaresque][picaresque] and sandbox style play in the old school tradition.
>
> It's a work in progress, but will eventually be published with lots of evocative art and flavor text, as well as audio vignettes (narrated again by [Chris Allen][chrisallen])

---

## [PSPowerHour: PowerShell Community Lightning Talks][pspowerhour] (_Cofounder, Cohost_)

The PSPowerHour is a community project helmed by myself and [WarrenFrame][warrenframe] to provide a platform and support for folks in the PowerShell community to give short (less than 10 minute) talks twice per month to a live virtual audience.

We encourage folks across the community to share what they've learned, rant about problems, and otherwise give back to the community - we have not _yet_ turned down a single talk submission.
This platform gives people a chance to practice public speaking, share knowledge, and build recognition in the community.

You can [submit a talk here][pspowerhour-submit] or review [open proposals here][pspowerhour-proposals] (they'll have a `proposal-open`, `proposal-constrained`, or `proposal-scheduled` label).

---

## [ChatterOps][chatterops] (_Founder, Host_)

ChatterOps is a tech-focused community show where we invite one or more primary guests, ask them questions, then open things up to the community for interaction.

I host the episodes and do most of the behind-the-scenes work for the show.

---

## PowerShell

> ### [ScriptAsService][scriptasservice] (_Maintainer_)
> This PowerShell module helps users take a looping PowerShell script and turn it into an executable that is installable as a service on a Windows machine.
> Primarily, my use case for this was for adding maintenance scripts which could be versioned, configured, and deployed sanely via configuration management.

> ### [Documentarian][documentarian] (_Maintainer_)
> This PowerShell module provides a [Plaster][plaster] template for scaffolding out [gitbook][gitbook] for project documentation.
> It's largely focused on PowerShell projects, but the primary goal is to make it easier for people to document their work by lowering the barriers to getting started.

> ### [PoshBot][poshbot] (_Contributor_)
> PoshBot is an _awesome_ chatbot written in PowerShell primarily by [Brandon Olin][olin].
> I've been involved mostly to provide bug reports, test, and help with documentation.

> ### [PoshSpec][poshspec] (_Contributor_)
> PoshSpec is a very useful PowerShell module written primarily by [Chris Hunt][hunt] which makes it easier to write infrastructure tests using [Pester][pester] and PowerShell.
> I've contributed a few test types and do so any time I discover a test type I need which doesn't already exist.

> ### [Vester][vester] (_Contributor_)
> Vester is a PowerShell module / project which makes it much, much easier to test VMWare infrastructure using PowerShell, written primarily by [Chris Wahl][wahl] and [Brian Bunke][bunke].
> It provides numerous default tests and a lot of ease-of-use improvements for folks.
> 
> My contributions have been almost entirely documentation and bug-reporting.

---

[bunke]:                 http://www.brianbunke.com/
[chatterops]:            https:///chatterops.org
[chrisallen]:            http://www.chrisallenvoice.com/
[devopscollective]:      https://devopscollective.org/
[documentarian]:         https://gitlab.com/michaeltlombardi/documentarian
[gitbook]:               https://www.gitbook.com/
[hunt]:                  https://www.automatedops.com/
[jeffhicks]:             https://twitter.com/JeffHicks
[mikefrobbins]:          https://twitter.com/mikefrobbins
[needful]:               https://michaeltlombardi.gitlab.io/needful-docs
[olin]:                  https://devblackops.io
[onramp-scholarship]:    https://powershell.org/summit/summit-onramp/onramp-scholarship/
[onramp]:                https://powershell.org/summit/summit-onramp/
[pentola]:               https://leanpub.com/pentola
[pester]:                https://github.com/pester/pester
[plaster]:               https://github.com/PowerShell/Plaster
[poshbot]:               https://github.com/poshbotio/PoshBot
[poshspec]:              https://github.com/Ticketmaster/poshspec
[psconfbook]:            https://leanpub.com/powershell-conference-book
[pspowerhour-proposals]: https://github.com/PSPowerHour/PSPowerHour/issues?q=is%3Aissue+is%3Aopen+sort%3Aupdated-desc
[pspowerhour-submit]:    https://github.com/PSPowerHour/PSPowerHour/issues/new
[pspowerhour]:           https://www.youtube.com/c/PSPowerHour
[pssummit-2017]:         https://eventloom.com/event/home/summit2017
[pwshop]:                https://leanpub.com/c/pwshop
[scriptasservice]:       https://gitlab.com/michaeltlombardi/ScriptAsService
[vester]:                https://github.com/WahlNetwork/Vester
[wahl]:                  http://wahlnetwork.com/